MF_DIR=$(dir $(realpath $(firstword $(MAKEFILE_LIST))))

include $(MF_DIR)mkfile_config.txt

all: parkourGame

parkourGame: $(BUILD_OBJECTS) $(SOURCE_DIR)Main.cpp $(HEADER_DIR)Main.hpp
	$(CC) $(EXEC_FLAG) -o $(BIN_DIR)parkourGame $(SOURCE_DIR)Main.cpp $(BUILD_OBJECTS) $(CFLAGS) $(LIBS)

clean:
	#@rm -Rv $(BUILD_OBJECTS)

mrproper: clean
	@rm -v $(BIN_DIR)parkourGame

install: parkourGame

uninstall: