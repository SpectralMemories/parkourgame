# ParkourGame
This is a temporary name, a real name will be chosen later on

# Dependencies
## SFML
Any modern implementation of SFML will work. I test with 2.5.1, so try it if you run into trouble

## C++ Standart Library (stdc++)
This really should come with your OS. If not, uhhh... You got other problems to fix

# Compiling
Simply use the Makefile. Tested on Arch Linux, works fine.

# Binary Releases
Will come later, for both Linux (Deb and PKGBUILD) and Windows (7+)
